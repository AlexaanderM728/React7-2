import React from 'react';
import Header from './components/Header';
import Navigation from './components/Navigation';
import BrowserList from './components/BrowserList';
import Footer from './components/Footer';

import data from './data.json';


const App = () => {
  const browsers = data.browsers;

  return (
    <div>
      <Header />
      <Navigation />

        <h2>Popular web browser</h2>
        <BrowserList browsers={browsers}/>
        <Footer/>
    </div>
  );
};

export default App;
